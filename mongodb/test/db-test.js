'use strict'
const test = require('ava')
const Db = require('../')
const fixtures = require('./fixtures')

/**
 * @description setup the test database.
 */
test.beforeEach('setup database', async t => {
  const dbName = 'nodo_test'
  const db = new Db()
  await db.connect()
  t.context.db = db
  t.context.dbName = dbName
  t.true(db.connected, 'should be connected')
})

test('test connection to database', async t => {
  const dbName = 'entrete_test'
  const db = new Db()
  await db.connect()
  t.context.db = db
  t.context.dbName = dbName
  t.true(db.connected, 'should be connected')
  await db.disconnect()
  t.false(db.connected, 'should not be connected')
})

/**
 * @description test the saving of a event in database.
 */
test('save a event', async t => {
  let db = t.context.db
  t.is(typeof db.savePublication, 'function', 'saveEvent is a function')
  let event = fixtures.getEvent()

  let created = await db.saveEvent(event, 'news')
  // t.true(created, 'should have registered a publication')
  t.is(typeof created, 'object')

  let query = {
    name: event.name
  }
  /*let result = await db.deletePublication(query)
  t.true(result, 'should delete publication')*/
})

test.todo('test eventos')
