'use strict'
/*
  This model represents a Event
*/
const mongoose = require('bluebird').promisifyAll(require('mongoose'))
const timestamps = require('mongoose-timestamp')

const Schema = mongoose.Schema

let EventSchema = new Schema({
  categoryId: {
    type: String,
    index: true
  },
  date: {
    type: Date,
    index: true
  },
  publicId: {
    type: String,
    index: true
  },
  popularity: Number,
  name: String,
  description: String,
  shortDescription: String,
  imgURL: String,
  eventURL: String,
  address: String,
  location: {
    type: { type: String },
    coordinates: []
  }
})

EventSchema.index({ location: '2dsphere' })
EventSchema.plugin(timestamps)

module.exports = mongoose.model('Event', EventSchema)

/*
  QUERY WITH NEAR BE LIKE:

  Message.find({
    location: {
      $near: {
        $maxDistance: 1000,
        $geometry: {
          type: "Point",
          coordinates: [long, latt]
        }
      }
    }
  }).find((error, results) => {
    if (error) console.log(error);
    console.log(JSON.stringify(results, 0, 2));
  });
*/
