'use strict'
/*
  This model represents a Event
*/
const mongoose = require('bluebird').promisifyAll(require('mongoose'))
const timestamps = require('mongoose-timestamp')

const Schema = mongoose.Schema

let CategorySchema = new Schema({
  name: String,
  publicId: {
    type: String,
    index: true
  }
})

CategorySchema.plugin(timestamps)

module.exports = mongoose.model('Category', CategorySchema)
